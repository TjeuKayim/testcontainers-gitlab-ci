plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin on the JVM
    id("org.jetbrains.kotlin.jvm").version("1.3.11")
    // Apply the application to add support for building a CLI application
    application
}

repositories {
    jcenter()
}

dependencies {
    // Kotlin JDK 8 standard library
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Testing
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.4.2")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:5.4.2")

    // Testcontainers
    testImplementation("org.testcontainers:selenium:1.11.2")

    // Selenium
    testImplementation("org.seleniumhq.selenium:selenium-chrome-driver:3.141.59")

    // Logging
    runtime("org.slf4j:slf4j-simple:1.7.26")
    testImplementation("io.github.microutils:kotlin-logging:1.6.26")
}

application {
    // Define the main class for the application
    mainClassName = "com.gitlab.tjeukayim.testcontainers.AppKt"
}

tasks.test {
    useJUnitPlatform()
}

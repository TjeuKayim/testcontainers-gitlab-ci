package com.gitlab.tjeukayim.testcontainers

import org.openqa.selenium.chrome.ChromeOptions
import org.testcontainers.containers.BrowserWebDriverContainer
import kotlin.test.Test
import kotlin.test.assertTrue

class SeleniumTest {
    private val chrome: BrowserWebDriverContainer<*> = BrowserWebDriverContainer<Nothing>()
        .withCapabilities(ChromeOptions())

    @Test
    fun `Simple plain Selenium test`() {
        chrome.start()
        val driver = chrome.getWebDriver()

        driver.get("https://wikipedia.org")
        val searchInput = driver.findElementByName("search")

        searchInput.sendKeys("Rick Astley")
        searchInput.submit()

        val otherPage = driver.findElementByPartialLinkText("Rickrolling")
        otherPage.click()

        val expectedTextFound = driver.findElementsByCssSelector("p")
            .stream()
            .anyMatch { element -> element.text.contains("meme") }

        assertTrue(expectedTextFound, "The word 'meme' is found on a page about rickrolling")
    }
}

package com.gitlab.tjeukayim.testcontainers

import com.google.common.util.concurrent.Uninterruptibles
import org.rnorth.ducttape.unreliables.Unreliables
import org.testcontainers.containers.BindMode.READ_ONLY
import org.testcontainers.containers.GenericContainer
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.Socket
import java.util.concurrent.TimeUnit
import kotlin.test.Test
import kotlin.test.assertEquals

class FileMountingTest {
    private val alpineClasspathResource: KGenericContainer = KGenericContainer("alpine:3.8")
        .withExposedPorts(80)
        .withClasspathResourceMapping("mappable-resource/test-resource.txt", "/content.txt", READ_ONLY)
        .withCommand("/bin/sh", "-c", "while true; do cat /content.txt | nc -l -p 80; done")

    @Test
    fun customClasspathResourceMappingTest() {
        alpineClasspathResource.start()
        val line = getReaderForContainerPort80(alpineClasspathResource).readLine()

        assertEquals(
            "FOOBAR", line,
            "Resource on the classpath can be mapped using calls to withClasspathResourceMapping"
        )
    }

    private fun getReaderForContainerPort80(container: GenericContainer<*>): BufferedReader {

        return Unreliables.retryUntilSuccess(10, TimeUnit.SECONDS) {
            Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS)

            val socket = Socket(container.getContainerIpAddress(), container.getFirstMappedPort())
            BufferedReader(InputStreamReader(socket.getInputStream()))
        }
    }
}

private class KGenericContainer(imageName: String) : GenericContainer<KGenericContainer>(imageName)

package com.gitlab.tjeukayim.testcontainers

import mu.KLogging
import kotlin.test.Test

class LogTest {
    private companion object : KLogging()

    @Test
    fun `Logger should log to console`() {
        logger.info { "Hello World" }
    }
}
